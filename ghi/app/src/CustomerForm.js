import React, { useState }  from 'react';


export default function CustomerForm(){
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [phone_number, setPhoneNumber] = useState('');
    const [address, setAddress] = useState('');

    const handleFirstName = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }

    const handleLastName = (event) => {
        const value = event.target.value;
        setLastName(value);
    }

    const handlePhoneNumber = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }

    const handleAddress = (event) => {
        const value = event.target.value;
        setAddress(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = first_name;
        data.last_name = last_name;
        data.phone_number = phone_number;
        data.address = address;

        const customerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);

        if (response.ok){
            const newCustomer = await response.json();
            setFirstName('');
            setLastName('');
            setPhoneNumber('');
            setAddress('');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form onSubmit={handleSubmit} id="add-customer-form">
                        <div className="form-floating mb-3">
                            <input required value={first_name} onChange={handleFirstName} placeholder="First Name" type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required value={last_name} onChange={handleLastName} placeholder="Last Name" type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required value={phone_number} onChange={handlePhoneNumber} placeholder="Phone Number" type="tel" name="phone_number" id="phone_number" className="form-control" />
                            <label htmlFor="phone_number">Phone Number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input required value={address} onChange={handleAddress} placeholder="Address" type="text" name="address" id="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <button className="btn btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
