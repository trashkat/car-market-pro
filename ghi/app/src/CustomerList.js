import React, { useState, useEffect } from 'react';


export default function CustomerList(){

    const [customers, setCustomers] = useState([]);

    const fetchData = async () => {
        const customerUrl = "http://localhost:8090/api/customers";
        const response = await fetch(customerUrl);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    };

    useEffect(() => {
        fetchData();

    }, []);

    return (
        <div className="container my-3">
            <h1>Customers</h1>
            <table className="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                        <th scope="col">Phone Number</th>
                        <th scope="col">Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(customer => {
                        return (
                            <tr key={ customer.id }>
                                <td>{ customer.first_name }</td>
                                <td>{ customer.last_name }</td>
                                <td>{ customer.phone_number }</td>
                                <td>{ customer.address }</td>
                                <td><button onClick={() => {
                            const Url = `http://localhost:8090/api/customers/${customer.id}/`
                            fetch(Url, { method: "DELETE"}).then((response) => {
                            if(!response.ok){
                                throw new Error('Something went wrong');
                            }
                            window.location.reload(false);
                            })
                            .catch((e) => {
                                console.log(e);
                            });
                        }} className="btn btn-danger">Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
