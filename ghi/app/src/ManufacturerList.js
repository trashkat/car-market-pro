import React, { useState, useEffect } from "react";


function ManufacturerList() {

    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(manufacturerUrl);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        } else {
            alert("Could not fetch manufacturers")
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="container my-3">
        <h1>Manufacturers</h1>
        <table className="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Name</th>
            </tr>
        </thead>
        <tbody>
            {manufacturers.map(manufacturer => {
                return (
                    <tr key={manufacturer.id}>
                        <td>{ manufacturer.name }</td>
                        <td><button onClick={() => {
                        const Url = `http://localhost:8100/api/manufacturers/${manufacturer.id}/`
                        fetch(Url, { method: "DELETE"}).then((response) => {
                        if(!response.ok){
                            throw new Error('Something went wrong');
                        }
                        window.location.reload(false);
                        })
                        .catch((e) => {
                            console.log(e);
                        });
                    }} className="btn btn-danger">Delete</button></td>
                    </tr>
                )
            })}
        </tbody>
        </table>
        </div>
    )
}


export default ManufacturerList
