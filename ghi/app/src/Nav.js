import { NavLink } from 'react-router-dom';
import React, { useState } from 'react';
import { Link } from 'react-router-dom';


export default function Nav(props) {

    const Header = () => {
        return (
            <>
                <NavLink className="navbar-brand" to="/">CarCar</NavLink>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
            </>
        )
    }

    const menuItems = [
        {
            title: "Manufacturers",
            url: "/manufacturers",
            submenu: [
                {
                    title: "Manufacturers List",
                    url: "/manufacturers"
                },
                {
                    title: "Add a Manufacturer",
                    url: "/manufacturers/new"
                }
            ]
        },
        {
            title: "Models",
            url: "/models",
            submenu: [
                {
                    title: "Models List",
                    url: "/models"
                },
                {
                    title: "Add a Model",
                    url: "/models/new"
                }
            ]
        },
        {
            title: "Automobiles",
            url: "/autmobiles",
            submenu: [
                {
                    title: "Automobile List",
                    url: "/automobiles"
                },
                {
                    title: "Add an Automobile",
                    url: "/automobiles/new"
                }
            ]
        },
        {
            title: "Salespeople",
            url: "/salespeople",
            submenu: [
                {
                    title: "Salespeople List",
                    url: "/salespeople"
                },
                {
                    title: "Add a Salesperson",
                    url: "/salespeople/new"
                }
            ]
        },
        {
            title: "Customers",
            url: "/customers",
            submenu: [
                {
                    title: "Customer List",
                    url: "/customers"
                },
                {
                    title: "Add a Customer",
                    url: "/customers/new"
                }
            ]
        },
        {
            title: "Sales",
            url: "/sales",
            submenu: [
                {
                    title: "Sales List",
                    url: "/sales"
                },
                {
                    title: "Add a Sale",
                    url: "/sales/new"
                },
                {
                    title: "Sales History",
                    url: "/sales/history"
                }
            ]
        },
        {
            title: "Technicians",
            url: "/technicians",
            submenu: [
                {
                    title: "Technicians List",
                    url: "/technicians"
                },
                {
                    title: "Add a Technician",
                    url: "/technicians/new"
                }
            ]
        },
        {
            title: "Services",
            url: "/services",
            submenu: [
                {
                    title: "Service Appointments",
                    url: "/services"
                },
                {
                    title: "Create a Service",
                    url: "/services/new"
                },
                {
                    title: "Service History",
                    url: "/services/history"
                }
            ]
        },
    ];

    const Dropdown = ({ submenus, dropdown }) => {
        return (
            <ul className={`dropdown-menu ${dropdown ? "show" : ""}`}
                aria-labelledby="defaultDropdown">
                { submenus.map((submenu, index) => (
                    <li className="dropdown-item" key={ index }>
                        <Link className="dropdown-item" to={ submenu.url }>{ submenu.title }</Link>
                    </li>
                ))}
            </ul>
        );
    };


    const MenuItems = ({ items }) => {
        const [dropdown, setDropdown] = useState(false);

        const handleDropdown = event => {
            setDropdown((prev) => !prev)
        };

        return (
            <li className="nav-item">
                { items.submenu ? (
                    <>
                        <button className="btn btn-outline-light dropdown-toggle "
                                onClick={ handleDropdown }
                                type="button"
                                id="defaultDropdown"
                                data-bs-auto-close="true">
                            {items.title}{" "}
                        </button>
                        <Dropdown submenus={ items.submenu } dropdown={dropdown}/>
                    </>
                ) : (
                    <Link className="nav-link" to={ items.url }>{ items.title }</Link>
                )}
            </li>
        );
    };

    const NavBar = () => {
        return (
            <>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        {menuItems.map((menu, index) => {
                            return <MenuItems items={ menu } key={ index } />
                        })}
                    </ul>
                </div>
            </>
        )
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-success">
            <div className="container-fluid">
                <Header />
                <NavBar />
            </div>
        </nav>
    )

}
