import React, { useState, useEffect } from "react";

function ServiceHistory() {

    const [services, setServices] = useState([]);
    const [searchInput, setSearchInput] = useState("");

    const fetchData = async () => {
        const appointmentUrl = "http://localhost:8080/api/appointments";
        const response = await fetch(appointmentUrl);

        if (response.ok) {
            const data = await response.json();
            setServices(data.appointments.filter(appointment => (appointment.status === "CANCELLED" || appointment.status === "FINISHED")));
        } else {
            alert("Failed to fetch appointment data!")
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleChange = (e) => {
        e.preventDefault();
        setSearchInput(e.target.value);
    };

    if (searchInput.length > 0) {
        services.filter((appointment) => {
            return appointment.vin.match(searchInput);
        });
    }


    return (
        <div className="container my-3">
        <h1>Service History</h1>
        <div className="container my-4">
            <input type="search" placeholder="Search by VIN" onChange={handleChange} value={searchInput} />
        </div>
        <table className="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Is VIP?</th>
                    <th>Customer</th>
                    <th>Date & Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                {services.filter(appointment => {
                    if (searchInput === '') {
                        return appointment;
                    } else if (appointment.vin.toLowerCase().includes(searchInput.toLowerCase())) {
                        return appointment;
                    }
                }).map((appointment, index) => {
                    return (
                        <tr key={index}>
                        {/* <tr key={appointment.id}> */}
                            <td>{ appointment.vin }</td>
                            <td>{ appointment.VIP }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ new Date(appointment.date_time).toLocaleString() }</td>
                            <td>{ appointment.technician.first_name + " " + appointment.technician.last_name }</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.status }</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </div>
    )
}


export default ServiceHistory
