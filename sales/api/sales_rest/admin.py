from django.contrib import admin
from .models import Sale, Salesperson


@admin.register(Sale)
class SaleAdmin(admin.ModelAdmin):
    list_display = (
        "price",
        "auto",
        "salesperson",
        "customer",
        "id"
    )


@admin.register(Salesperson)
class SalespersonAdmin(admin.ModelAdmin):
    list_display = (
        "employee_id",
        "first_name",
        "last_name"
    )
