from django.urls import path
from .views import list_technicians, show_technician, list_appointments, cancel_appointment, finish_appointment, show_appointment


urlpatterns = [
    path("technicians/", list_technicians, name="create_technician"),
    path("technicians/<int:id>/", show_technician, name="delete_technician"),
    path("appointments/", list_appointments, name="create_technician"),
    path("appointments/<int:id>/", show_appointment, name="delete_appointment"),
    path("appointments/<int:id>/cancel/", cancel_appointment, name="cancel_appointment"),
    path("appointments/<int:id>/finish/", finish_appointment, name="finish_appointment"),
]
