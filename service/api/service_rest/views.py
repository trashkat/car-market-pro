from django.http import JsonResponse
from django.db import IntegrityError
from django.views.decorators.http import require_http_methods
import json
from service_rest.models import AutomobileVO, Technician, Appointment
from service_rest.encoders import (
    AppointmentEncoder,
    TechnicianDetailEncoder,
    TechnicianListEncoder,
)


@require_http_methods(['GET', 'POST'])
def list_technicians(request):
    if request.method == 'GET':
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianDetailEncoder,
                safe=False,
            )
        except IntegrityError:
            return JsonResponse(
                {'error': 'Employee with that ID already exists!'},
                status=400
            )


@require_http_methods(['DELETE'])
def show_technician(request, id):
    if request.method == 'DELETE':
        count, _ = Technician.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(['GET', 'POST'])
def list_appointments(request):
    if request.method == 'GET':
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)

        try:
            technician_id = content["technician_id"]
            technician = Technician.objects.get(id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician."},
                status=400
            )
        try:
            appointment_vin = content["vin"]
            appointment_vin = AutomobileVO.objects.get(vin=appointment_vin)
            content["VIP"] = "YES"
        except AutomobileVO.DoesNotExist:
            content["VIP"] = "NO"

        appointment = Appointment.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )


@require_http_methods(['DELETE'])
def show_appointment(request, id):
    if request.method == 'DELETE':
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})


@require_http_methods(["PUT"])
def cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Appointment ID."},
            status=400
        )

    appointment.cancel()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )


@require_http_methods(["PUT"])
def finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
    except Appointment.DoesNotExist:
        return JsonResponse(
            {"message": "Invalid Appointment ID."},
            status=400
        )

    appointment.complete()
    return JsonResponse(
        appointment,
        encoder=AppointmentEncoder,
        safe=False,
    )
